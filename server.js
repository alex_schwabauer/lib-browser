/// <reference path='typings/express/express.d.ts' />

var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var mailer = require("nodemailer").createTransport("SMTP", {
    host: "headgame.draco.uberspace.de",
    port: 587,
    auth: {
        user: "headgame",
        pass: "unnormallangersatzduhurensohn"
    }
});

var config = require('./config.json')[process.env.NODE_ENV || 'development'];

//app.use('/', express.static(__dirname + config.static_dir + 'index.html'));
//app.use(require('connect-livereload')());
app.use(express.static(__dirname + config.static_dir));
app.use(bodyParser.urlencoded({ extended: false }));

app.set('views', __dirname + config.static_dir + "/views");
app.set('view engine', 'jade');

app.get('/', function (req, res) {
    res.render('index', { active: 0 });
});
app.get('/about', function (req, res) {
    res.render('about', { active: 1 });
});
app.get('/contact', function (req, res) {
    res.render('contact', { active: 2 });
});

app.post('/contact', function (req, res) {
    var data = req.body;

    if (data.email == undefined || data.name == undefined || data.message == undefined) {
        console.warn("email, name or message in '\contact' is undefined.");
        mailer.sendMail({
            from: "Codeneric <noreply@codeneric.com>",
            to: "schwabauer_alexander@web.de, denis.golovin@rwth-aachen.de",
            subject: "Warn: Anfrage",
            html: "email, name or message in '\contact' is undefined."
        }, function (error, success) {
            if (error) {
                console.warn("Sending message failed:", error);
            }
        });

        return;
    }
    mailer.sendMail({
        from: "Codeneric <noreply@codeneric.com>",
        to: data.email,
        subject: "Ihre Anfrage wird verarbeitet",
        html: "//English version below\n<h3>Hallo " + data.name + ",</h3><p>Ihre Anfrage ist bei uns eingegangen und wird nun von uns verarbeitet.<br><br>" + "Wir werden Sie umgehend kontaktieren.<br><br>Viele Grüße,<br>Codeneric GbR - Denis Golovin, Alexander Schwabauer</p>" + "\n\n\n\n###\n\n\n\n<h3>Hello " + data.name + ",</h3><p>We have received your request and will respond soon.<br><br>" + "<br>Best Regards,<br>Codeneric GbR - Denis Golovin, Alexander Schwabauer</p>"
    }, function (error, success) {
        if (error) {
            console.log("Sending message failed (client):", error);
            //res.send(500);
        }
    });

    mailer.sendMail({
        from: "Codeneric <noreply@codeneric.com>",
        to: "denis@codeneric.com",
        subject: "Neue Kundenanfrage",
        html: "Neue Anfrage von: <ul><li>Name: " + data.name + "</li><li>Company:" + data.company + "</li><li>Email:" + data.email + "</li><li>Message: " + data.message + "</li></ul>"
    }, function (error, success) {
        if (error) {
            console.log("Sending message failed:", error);
            res.sendStatus(500);
        }

        res.render('thanks', { active: 2 });
    });
});

var server = app.listen(config.port, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});
