angular.module('app', [])
    .directive('Animation',  function($window) {
        return {
            restrict: 'C',
            link: function(scope, elem, attr) {
                var height = 0;

                if(attr.fullHeight)
                    height = elem.height();

                function isVisible() {
                    return (elem.offset().top + height <= $window.pageYOffset + $window.innerHeight);
                }

                function applyClass() {
                    if(isVisible()) {
                        elem.addClass("in");
                        return true;
                    }
                    return false;
                }


                if(applyClass())
                    return;

                angular.element($window).bind("scroll.Animation", function() {
                    if(applyClass())
                        angular.element($window).unbind("scroll.Animation");
                });
            }
        }


    })
    .directive('ccScroll',  function($location) {
        return {
            restrict: 'A',
            link: function(scope, elem, attr) {
                console.log("linked!");
                elem.bind("click", function () {
                    setTimeout(function () {
                        $(document.body).animate(
                            { scrollTop: $("#" + $location.path().substr(1)).position().top + "px"},
                            500, 'swing');
                    },0);

                })
            }
        }


    });

